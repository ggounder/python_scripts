import time
import pyautogui
import random
from itertools import chain
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

appurl = 'https://wsldctpgweb.water.internal/NView/?config=appConfig/'

jsonurl = 'nvms.json'

fullurl = 'https://watercare.maps.arcgis.com/apps/webappviewer/index.html?id=cb728d53843a437894512d5f5a988184'

widgetName = 'Metadata'

options = Options()
options.add_argument("start-maximized")
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Users\ggounder\Downloads\BitbucketProjects\python_scripts\chromedriver.exe')
driver.get(appurl+jsonurl)

time.sleep(10)
driver.find_element_by_xpath("//div[contains(@class, 'nextBtn jimu-corner-right enabled')]").click()
time.sleep(1)

driver.find_element_by_xpath("//div[contains(@settingid, '" + widgetName + "')]").click()
time.sleep(1)

list_of_elements = driver.find_elements_by_xpath("//font[contains(@color, '#000000')]/parent::div/following-sibling::div//a")
title = [name.text for name in list_of_elements]
link = [name.get_attribute('href') for name in list_of_elements]
combined_list = dict(zip(title, link))
print(combined_list)
print('Metadata Widget Tested')







