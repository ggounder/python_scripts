import time
import pyautogui
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

appurl = 'https://wsldctpgweb.water.internal/NView/?config=appConfig/'

jsonurl = ['nvcu.json',
           'nvge.json',
           'nvop.json',
           'nvms.json',
           'nvin.json',
           'nvpr.json',
           'nvdt.json',
           'nvmd.json',
           'nvwo.json']    
        
            

def runtest(jsonurl):

    options = Options()
    options.add_argument("start-maximized")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Users\ggounder\Downloads\BitbucketProjects\python_scripts\chromedriver.exe')
    driver.get(appurl + jsonurl)

    latlongdict = {'1753520.83':'5917046.79',
		   '1745312.40':'5919895.39',
                   }											

    pyautogui.press('f11')

    def loadandsearch():
        tries = 3
        
        time.sleep(15)

        for i in range(tries):
            try:
                driver.find_element_by_xpath("//div[@settingid='widgets_eLocate_Widget']").click()                
            except KeyError as e:
                if i < tries - 1:
                    continue
                else:
                    raise
            break
               
        time.sleep(2)

        ASWPClick = driver.find_elements_by_xpath('//*[@id="widget_dijit_form_TextBox_1"]')[0]

        ASWPClick.click()

        time.sleep(2)

        pyautogui.typewrite(easting, interval=0.25)

        time.sleep(2)

        ASWPClickNorth = driver.find_elements_by_xpath('//*[@id="widget_dijit_form_TextBox_2"]')[0]

        ASWPClickNorth.click()

        pyautogui.typewrite(northing, interval=0.25)

        time.sleep(2)
        
        ASResult = driver.find_elements_by_xpath('//*[@id="jimu_dijit_ViewStack_0"]/div[4]/div[5]') [0]

        ASResult.click()

        time.sleep(2)

        ClearResults = driver.find_elements_by_xpath('//*[@id="widgets_PopupPanel_Widget"]/div[1]/div[2]/a')[0]

        ClearResults.click()

        time.sleep(1)

        ASClose =  driver.find_elements_by_xpath('//*[@id="widgets_eLocate_Widget_panel"]/div[1]/div[4]')[0]

        ASClose.click()

        time.sleep(2)

        pyautogui.click(x=959.5, y=539.5)

        time.sleep(3)

        divClass = driver.find_elements_by_class_name("attrTable")               
        for row in divClass:
            print (row.text)
               
        time.sleep(3)
        
        pyautogui.press('f5')

    for easting,northing in latlongdict.items():
        loadandsearch()

    print ('Testing Completed for: ' + jsonurl)
    driver.quit()        

    
for x in jsonurl:
    runtest(x)    

print ('Testing Fully Complete Done')
