import time
import pyautogui
import random
from itertools import chain
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

appurl = 'https://wsldctpgweb.water.internal/NView/?config=appConfig/'

jsonurl =  ['nvcu.json',
           'nvge.json',
           'nvop.json',
           'nvms.json',
           'nvin.json',
           'nvpr.json',
            ]  

fullurl = 'https://watercare.maps.arcgis.com/apps/webappviewer/index.html?id=cb728d53843a437894512d5f5a988184'

def runtest(jsonurl):

    options = Options()
    options.add_argument("start-maximized")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Users\ggounder\Downloads\BitbucketProjects\python_scripts\chromedriver.exe')
    driver.get(appurl+jsonurl)

    time.sleep(10)

    list_of_widgets = driver.find_elements_by_xpath("//div[contains(@class, 'iconGroup')]/div")
    widgets = [name.get_attribute('settingid') for name in list_of_widgets]
    print (widgets)

    def openingWidgets(widgets):
        if widgets == 'widgets_Metadata_Widget':
            driver.find_element_by_xpath("//div[contains(@class, 'nextBtn jimu-corner-right enabled')]").click()
            print ("Opening " + widgets)
            driver.find_element_by_xpath("//div[contains(@settingid, '" + widgets + "')]").click()
            time.sleep(2)
            print ("Closing " + widgets)
            driver.find_element_by_xpath("//div[contains(@settingid, '" + widgets + "')]").click()
        elif widgets == 'widgets_AttributeTable_Widget_':
            driver.find_element_by_xpath("//div[contains(@class, 'nextBtn jimu-corner-right enabled')]").click()
            print ("Opening " + widgets)
            driver.find_element_by_xpath("//div[contains(@settingid, '" + widgets + "')]").click()
            time.sleep(2)
            print ("Closing " + widgets)
            driver.find_element_by_xpath("//div[contains(@settingid, '" + widgets + "')]").click()
        else:
            print ("Opening " + widgets)
            driver.find_element_by_xpath("//div[contains(@settingid, '" + widgets + "')]").click()
            time.sleep(2)
            print ("Closing " + widgets)
            driver.find_element_by_xpath("//div[contains(@settingid, '" + widgets + "')]").click()

    for x in widgets:
        openingWidgets(x)

    print ("Testing Complete for " + jsonurl)
    driver.quit()   
    
for x in jsonurl:
    runtest(x)    

print ("All Testing Complete")
    







