#script must be run in Python 3

'''this script based upon: "https://community.esri.com/thread/208467-download-feature-service-as-file-geodatabase"'''
#note: This will delete a temporary geodatabase created in ArcGIS onlne via this script


import time
import os
import zipfile
import arcgis.gis
import datetime
import errno
from zipfile import ZipFile


released = {
				'Cape Hill Project' : 'dfde0da40431404798e6d2b17ee56f4a',
                'Clarks Beach Public Issue' : '00c7d3ce6dbd446ab7c77cb236dd8037',
                'Clarks Beach Private Issue' : '4c9efda0487c4179975642194cfb785b',
                'Clarks Beach Property Inspection' : 'f278c83d7a184c4884b2a3c450aead9e',
                'Clarks Beach Property Inspection Area' : '0742ba574901435a91bbcaa4e276b153',
                'Jutland Road Property Inspection' : '030e46fa2f6b463c8a5b7ffd10df2c6f',
                'Jutland Road Property Inspection Area' : '4526176f47294aa4b6f348ac098aec1f',
                'McLean Ave Project' : 'b4e7b5b8055f46b2812aa54517750929',
                'Mellons Bay Investigations' : 'e31eac12dc9c4182bb1fa1b6a5800dc1',
                'St Marys Bay Project' : '629f263250a64ceb9d14bf82e7a657a9',
                'St Marys Bay Public Issue' : '29449d45e0ab44c09a480b1fe8558078',
                'St Marys Customer Knowledge' : '24e1f003b47a48ebad07ed84e858ccd4',
                'St Marys Bay Seperation Cost Estimate' : 'a191b38d00dc435db373890f76f795b4',
                'Jutland Road Private Issue' : 'cdcb8f16f1df423c8537a1e68493beaf',
                'Jutland Road Public Issue' : '8a80feb0e13a4992ab68d772313a927b',
                'Te Atatu Project' : '2ce2d147fb514320987a6fb0daae3a98',
                'Waiheke Island' : 'a3dc609eedf741c0ae5fdc34c8e32947',
                'Weymouth Project' : '1dbc6354c58e4dcf828fb525b3f6d9bf',
                'Catchments' : '26a030e7508e4a45a0a946e179136818',
                'Wyllie Road Project' : 'ad4308c850d0467e8a009bf03ebce0c2',
	}

now = datetime.datetime.now()
day= now.strftime('%Y%m%d')

uname = input("Enter your username")
pword = input("Enter your password")

def ExportAGOLdata():
    ### *** modify these four lines ***
    outputFolder="C:/Users/ggounder/Downloads/" + day + "/"+ key  #where the GDB will be extracted to
    try:
        os.makedirs(outputFolder)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    print("Output Folder:"+ outputFolder)

    gis = arcgis.GIS("http://watercare.maps.arcgis.com", uname, pword) #replace these with your credentials
    item_id=val #repace with the service_ID of the hosted feature layer to download
    GDBname = "Temp_FGB" # name of the temporary GDB to be saved in ArcGIS Online

    AGOLitem = gis.content.get(item_id)

    print ("Exporting Hosted Feature Layer...")
    AGOLitem.export(GDBname,'File Geodatabase', parameters=None, wait='True')
    time.sleep(10)#add 10 seconds delay to allow export to complete

    search_fgb = gis.content.search(query = "title:{}*".format(GDBname )) #find the newly created file geodatabase in ArcGIS online
    fgb_item_id = search_fgb[0].id
    fgb = gis.content.get(fgb_item_id)
    fgb.download(save_path=outputFolder) #download file gdb from ArcGIS Online to your computer

    print ("Zipping exported geodatabase for download...")

    '''while statement runs until a valid zipped file is created'''
    ## randomly the output is a 1 KB file that is not a valid zipped file.
    ## The while statement forces a valid zipped file to be created.
    zipfullpath=os.path.join(outputFolder,GDBname+".zip")#full path to the zipped file once it is downloaded to your computer
    while  zipfile.is_zipfile(zipfullpath)==False:
        fgb.download(save_path=outputFolder)
    zf = ZipFile(os.path.join(outputFolder,GDBname+ ".zip"))

    print ("Extracting zipped file to geodatabase...")
    zf.extractall(path=os.path.join(outputFolder)) #note: GDB file name is completely random ex. 225eddc131ad4f8cbf4c54e1414bd129.gdb)
    ##a new gdb is created each time the line above is run

    #get names of files in zipped folder.
    with ZipFile(zipfullpath, 'r') as f:
        FilenamesInZip=f.namelist()
        for name in FilenamesInZip:
            if '.gdb/' in name:
                gdbname=str(name).split("/", 1)[0]
    print ("Exported geodatabase is named: "+gdbname) #returns name of the exported gdb

    '''delete zipped file in ArcGIS Online '''
    del zf #deletes the zf vairable so the lock file goes away
    print("Deleting "+os.path.join(outputFolder,GDBname+".zip"))
    os.remove(zipfullpath) #deletes actual zipped file on your computer

    '''deleting hosted File Geodatabase'''
    ###  NOTE: This will delete the temporary File Geodatabase in ArcGIS Online
    print("Deleting "+fgb.title+"("+fgb.type+")"+" from ArcGIS Online...")
    fgb.delete()


    print("Done!"+ key)

for key,val in released.items():
    ExportAGOLdata()
