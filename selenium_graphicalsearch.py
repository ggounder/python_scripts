import time
import pyautogui
import random
from itertools import chain
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

appurl = 'https://wsldctpgweb.water.internal/NView/?config=appConfig/'

jsonurl = 'nvms.json'

fullurl = 'https://watercare.maps.arcgis.com/apps/webappviewer/index.html?id=cb728d53843a437894512d5f5a988184'

widgetName = 'GraphicalSearch'

options = Options()
options.add_argument("start-maximized")
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Users\ggounder\Downloads\BitbucketProjects\python_scripts\chromedriver.exe')
driver.get(appurl+jsonurl)

def click_drag():
     pyautogui.moveTo(800, 500)
     pyautogui.mouseDown(button= 'left')
     pyautogui.moveTo(1000, 700)
     pyautogui.mouseUp(button='left')

time.sleep(10)

driver.find_element_by_xpath("//div[contains(@settingid, '" + widgetName + "')]").click()
time.sleep(1)

source_element = driver.find_element_by_xpath("//div[contains(@class, 'jimu-panel jimu-launchpad-panel')]//div[contains(text(), 'Graphical Search')]")
ActionChains(driver).click_and_hold(source_element).move_by_offset(100, 0).release(source_element).perform()


list_of_elements = driver.find_elements_by_xpath('//div[@class="list-item-name task-name-div"]')
List = [name.text for name in list_of_elements]

def PicklistSearch(List):
    driver.find_element_by_xpath("//*[contains(text(), '" + List + "')]").click()
    time.sleep(2)  
    menu = driver.find_elements_by_xpath("//div[contains(@class, 'checkBtn')]") 
    if len(menu) > 0:
        driver.find_element_by_xpath("//div[contains(@class, 'checkBtn')]").click()
        WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//*[contains(@data-dojo-attach-point, 'listContainer')]/div[contains(@data-dojo-attach-point, 'listContent')]"))
        )
        drop_dowm_menus = driver.find_elements_by_xpath("//div[contains(@style, 'display: block') and @class='jimu-popup']//div[@class='item']/div[2]")
        service = [name.text for name in drop_dowm_menus]
        driver.find_element_by_xpath("//div[contains(@class, 'checkBtn')]").click()
        def runquery(service):
            print('Running the Query for ' + List + ' on ' + service)
            driver.find_element_by_xpath("//div[contains(@class, 'checkBtn')]").click()                                 
            WebDriverWait(driver, 30).until(
            EC.presence_of_element_located((By.XPATH, "//div[contains(@style, 'display: block') and @class='jimu-popup']//div[contains(text(), '" + service + "')]"))
            )
            driver.find_element_by_xpath("//div[contains(@style, 'display: block') and @class='jimu-popup']//div[contains(text(), '" + service + "')]").click()
            time.sleep(1)
            driver.find_element_by_xpath("//div[contains(@title, 'Extent')]").click()
            time.sleep(1)
            click_drag()
            time.sleep(1)
            driver.find_element_by_xpath("//div[contains(@class, 'jimu-btn btn-execute')]").click()
            time.sleep(7)
            driver.find_element_by_xpath("//div[contains(@class, 'homeContainer')]").click()
            time.sleep(2)
            driver.find_element_by_xpath("//div[contains(text(), 'Tasks')]").click()
            time.sleep(2)
        
        for m in service:
            runquery(m)
    else:
        print('Running the Query for ' + List)
        driver.find_element_by_xpath("//div[contains(@title, 'Extent')]").click()
        time.sleep(1)
        click_drag()
        time.sleep(1)
        driver.find_element_by_xpath("//div[contains(@class, 'jimu-btn btn-execute')]").click()
        time.sleep(7)
        driver.find_element_by_xpath("//div[contains(@class, 'homeContainer')]").click()
        time.sleep(2)
        driver.find_element_by_xpath("//div[contains(text(), 'Tasks')]").click()
        time.sleep(2)
    
    #random.shuffle(drop_dowm_menus)
    #driver.find_element_by_xpath("//div[contains(@class, 'checkBtn')]").click()
    #def runthesearch(m):
        #driver.find_element_by_xpath("//div[contains(@class, 'checkBtn')]").click()
        #print('Running the Query for ' + List + ' on ' + m.text)
        #time.sleep(2)
        #driver.find_element_by_xpath("//div[contains(@style, 'display: block') and @class='jimu-popup']//div[contains(text(), '" + m.text + "')]").click()
       # time.sleep(2)
        #driver.find_element_by_xpath("//div[contains(@class, 'jimu-btn btn-execute')]").click()
        #time.sleep(5)
        #WebDriverWait(driver, 60).until(
        #EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Tasks')]"))
        #)
        #driver.find_element_by_xpath("//div[contains(text(), 'Tasks')]").click()
        #time.sleep(2)
    #for m in drop_dowm_menus[:2]:
        #runthesearch(m)      

    driver.find_element_by_xpath("//div[contains(@class, 'back-arrow')]").click()
    time.sleep(5)        
  

for x in List:
    PicklistSearch(x)

print ('Script has completed')




