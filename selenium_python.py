import time
import pyautogui
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

url = ['https://wsldctpgweb.water.internal/NView/?config=appConfig/nvcu.json', 'https://wsldctpgweb.water.internal/NView/?config=appConfig/nvge.json']

def runtest(url):

    options = Options()
    options.add_argument("start-maximized")
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Users\ggounder\Downloads\BitbucketProjects\python_scripts\chromedriver.exe')
    driver.get(url)

    pyautogui.press('f11')

    def loadandsearch(Compkey):
        time.sleep(15)

        ZoomIn = driver.find_elements_by_xpath('//*[@id="widgets_ZoomSlider_Widget"]/div[1]')[0]

        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()

        time.sleep(5)

        ASIcon = driver.find_elements_by_xpath('//*[@id="uniqName_11_4"]/div[1]')[0]

        ASIcon.click()

        time.sleep(2)

        ASWP = driver.find_elements_by_xpath('//*[@id="widgets_AttributeSearch_Widget"]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/table/tbody/tr[5]/td[2]/div')[0]

        ASWP.click()

        time.sleep(1)

        ASWPClick = driver.find_elements_by_xpath('//*[@id="uniqName_0_3"]/table/tbody/tr[2]/td')[0]

        ASWPClick.click()

        time.sleep(2)

        pyautogui.typewrite(Compkey, interval=0.25)

        pyautogui.press('enter')

        time.sleep(2)

        ASResult = driver.find_elements_by_xpath('//*[@id="uniqName_0_4"]/div[1]/div[3]/table/tbody/tr/td/table/tbody/tr/td[2]') [0]

        ASResult.click()

        time.sleep(2)

        ClearResults = driver.find_elements_by_xpath('//*[@id="widgets_PopupPanel_Widget"]/div[1]/div[2]/a')[0]

        ClearResults.click()

        time.sleep(1)

        ASClose =  driver.find_elements_by_xpath('//*[@id="widgets_AttributeSearch_Widget_panel"]/div[1]/div[4]')[0]

        ASClose.click()

        time.sleep(2)

        ZoomIn.click()
        time.sleep(1)
        ZoomIn.click()
        time.sleep(1)
        ZoomIn.click()
        time.sleep(1)

        pyautogui.click(x=959.5, y=539.5)

        time.sleep(2)

        PopupTable = driver.find_elements_by_xpath('//*[@id="esri_dijit__PopupRenderer_6"]/div[2]/div[3]/table')
        for row in PopupTable:
            print(row.text)

        pyautogui.press('f5')

    def loadandsearchtran(EquipID):
        time.sleep(15)

        ZoomIn = driver.find_elements_by_xpath('//*[@id="widgets_ZoomSlider_Widget"]/div[1]')[0]

        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()
        ZoomIn.click()

        time.sleep(5)

        ASIcon = driver.find_elements_by_xpath('//*[@id="uniqName_11_4"]/div[1]')[0]

        ASIcon.click()

        time.sleep(2)

        ASWP = driver.find_elements_by_xpath('//*[@id="widgets_AttributeSearch_Widget"]/div[1]/div[2]/div[1]/div[1]/div[1]/div[2]/table/tbody/tr[5]/td[2]/div')[0]

        ASWP.click()

        time.sleep(1)

        ASWPClick = driver.find_elements_by_xpath('//*[@id="widget_undefined_0"]/div[2]')[0]

        ASWPClick.click()

        time.sleep(2)

        pyautogui.typewrite(EquipID, interval=0.25)

        pyautogui.press('enter')

        time.sleep(2)

        ASResult = driver.find_elements_by_xpath('//*[@id="uniqName_0_4"]/div[1]/div[3]/table/tbody/tr/td/table/tbody/tr/td[2]') [0]

        ASResult.click()

        time.sleep(2)

        ClearResults = driver.find_elements_by_xpath('//*[@id="widgets_PopupPanel_Widget"]/div[1]/div[2]/a')[0]

        ClearResults.click()

        time.sleep(1)

        ASClose =  driver.find_elements_by_xpath('//*[@id="widgets_AttributeSearch_Widget_panel"]/div[1]/div[4]')[0]

        ASClose.click()

        time.sleep(2)

        ZoomIn.click()
        time.sleep(1)
        ZoomIn.click()
        time.sleep(1)
        ZoomIn.click()
        time.sleep(1)

        pyautogui.click(x=959.5, y=539.5)

        time.sleep(2)

        PopupTable = driver.find_elements_by_xpath('//*[@id="esri_dijit__PopupRenderer_6"]/div[2]/div[3]/table')
        for row in PopupTable:
            print(row.text)

        pyautogui.press('f5')


    Compkey = ['8751832', '8749669']

    for x in Compkey:
        loadandsearch(x)

    EquipID = ['10016923', '10016923']

    for x in EquipID:
        loadandsearchtran(x)

    driver.quit()
    print ('compleded testing for: ' + url)

for x in url:
    runtest(x)    

print ('Done')
