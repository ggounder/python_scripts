import time
import pyautogui
import random
from itertools import chain
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

appurl = 'https://wsldctdgweb.water.internal/NView/?config=appConfig/'

jsonurl = 'nvms.json'

fullurl = 'https://watercare.maps.arcgis.com/apps/webappviewer/index.html?id=cb728d53843a437894512d5f5a988184'

widgetName = 'Print'

options = Options()
options.add_argument("start-maximized")
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:\Users\ggounder\Downloads\BitbucketProjects\python_scripts\chromedriver.exe')
driver.get(appurl+jsonurl)

WebDriverWait(driver, 30).until(  
        EC.element_to_be_clickable((By.XPATH, "//div[contains(@settingid, '" + widgetName + "')]"))
        )
driver.find_element_by_xpath("//div[contains(@settingid, '" + widgetName + "')]").click()

WebDriverWait(driver, 30).until(
        EC.element_to_be_clickable((By.XPATH, "//td[contains(text(), 'Layout')]/parent::*//td[contains(@class, 'DownArrow')]"))
        )
driver.find_element_by_xpath("//td[contains(text(), 'Layout')]/parent::*//td[contains(@class, 'DownArrow')]").click()


WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//div[@id='dijit_form_Select_0_dropdown']//td[contains(@class, 'dijitReset dijitMenuItemLabel') and text()]"))
        )
list_of_layout = driver.find_elements_by_xpath("//div[@id='dijit_form_Select_0_dropdown']//td[contains(@class, 'dijitReset dijitMenuItemLabel') and text()]")
layout = [name.text for name in list_of_layout]
print(layout)
driver.find_element_by_xpath("//td[contains(text(), 'Layout')]/parent::*//td[contains(@class, 'DownArrow')]").click()


driver.find_element_by_xpath("//td[contains(text(), 'Format')]/parent::*//td[contains(@class, 'DownArrow')]").click()
WebDriverWait(driver, 30).until(
        EC.element_to_be_clickable((By.XPATH, "//div[@id='dijit_form_Select_1_dropdown']//td[contains(@class, 'dijitReset dijitMenuItemLabel') and text()]"))
        )
list_of_forms = driver.find_elements_by_xpath("//div[@id='dijit_form_Select_1_dropdown']//td[contains(@class, 'dijitReset dijitMenuItemLabel') and text()]")
form = [name.text for name in list_of_forms]
print(form)
driver.find_element_by_xpath("//td[contains(text(), 'Format')]/parent::*//td[contains(@class, 'DownArrow')]").click()

def select_layout (layout):
    driver.find_element_by_xpath("//td[contains(text(), 'Layout')]/parent::*//td[contains(@class, 'DownArrow')]").click()
    driver.find_element_by_xpath("//div[@id='dijit_form_Select_0_dropdown']//td[contains(@class, 'dijitReset dijitMenuItemLabel') and contains(text(), '"+ layout +"')]").click()
    def select_format (form):
        driver.find_element_by_xpath("//td[contains(text(), 'Format')]/parent::*//td[contains(@class, 'DownArrow')]").click()
        driver.find_element_by_xpath("//div[@id='dijit_form_Select_1_dropdown']//td[contains(@class, 'dijitReset dijitMenuItemLabel') and contains(text(), '"+ form +"')]").click()
        driver.find_element_by_xpath("//div[contains(@data-dojo-attach-point, 'printButtonDijit')]").click()
        print ("Printing " + form + " for " + layout)
    for x in form:
        select_format(x) 

for x in layout:
    select_layout(x)     

list_of_prints = driver.find_elements_by_xpath("//div[contains(@data-dojo-attach-point, 'printResultsNode')]/div")
docs = [name.get_attribute('widgetid') for name in list_of_prints]


def download_prints(docs):
    WebDriverWait(driver, 30).until(
        EC.element_to_be_clickable((By.XPATH, "//div[contains(@data-dojo-attach-point, 'printResultsNode')]/div[contains(@widgetid, '"+ docs +"')]"))
        )
    driver.find_element_by_xpath("//div[contains(@data-dojo-attach-point, 'printResultsNode')]/div[contains(@widgetid, '"+ docs +"')]").click()
    print ("Downloading Print for " + docs )

for x in docs:
    download_prints(x)    



print ("Completed Testing Print Widget")         

