import arcpy

gdb = r'F:\System\Geodatabase\DBConnections\Test\TestReplica\OS\TestOS@Net1Replica.sde'
arcpy.env.workspace = gdb

domains = ['D_AMS','D_NotApplicableInt','D_NotApplicableTxt']

for fds in arcpy.ListDatasets('','Feature'):
    print "DS: {}".format(fds)
    for fc in arcpy.ListFeatureClasses("*","",fds):
        print "\tFC: {}".format(fc)
        subtypes = arcpy.da.ListSubtypes(fc)
        # loops through feature class' subtypes one at a time
        for stcode, stdict in list(subtypes.items()):
            for stkey in list(stdict.keys()):
                # if there is a Subtype Field (that is, it is not an empty string)
                if not stdict['SubtypeField'] == '':
                    st_code = "'{}: {}'".format(stcode, stdict['Name'])
                    if stcode < 10 :
                        new_code = "'{}: {}'".format(stcode, stdict['Name'])
                        if stkey == 'FieldValues':
                            fields = stdict[stkey]
                            for field, fieldvals in list(fields.items()):
                                # if field has a domain
                                if not fieldvals[1] is None:
                                    # and the domain is in our list
                                    if fieldvals[1].name in domains:
                                        # delete the domain
                                        print("\t\t{} domain deleted from field {} in subtype {}".format(fieldvals[1].name, field, new_code))
                                        #arcpy.RemoveDomainFromField_management(in_table=fc, field_name=field, subtype_code=new_code)

print "Done."
