import psycopg2
from configparser import SafeConfigParser
config = SafeConfigParser()
print(config.sections())
username = config.get('wsldatahubadmin','username')
password = config.get('wsldatahubadmin','password')

try:
   connection = psycopg2.connect(user= username,
                                  password= password,
                                  host="wsldevcluster-aurora-postgresql-instance-1.ctshtbjm0f5d.ap-southeast-2.rds.amazonaws.com",
                                  port="5432",
                                  database="wsldatahub")
   cursor = connection.cursor()
   postgreSQL_select_Query = "select * from datahub_target.ips_assets"

   cursor.execute(postgreSQL_select_Query)
   print("Selecting rows from mobile table using cursor.fetchall")
   pg_records = cursor.fetchall()

   print("Print each row and it's columns values")
   for row in pg_records:
       print(row[0])
       print(row[1])
       print(row[2])
       print(row[3])
       print(row[4])
       print(row[5])


except (Exception, psycopg2.Error) as error :
    print ("Error while fetching data from PostgreSQL", error)

finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
