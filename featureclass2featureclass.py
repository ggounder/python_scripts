# Import the necessary modules
import arcpy
import time
import os
import datetime
import errno

# For the purposes of saving back-up data to a folder with today's date, obtain the date of the day that this script runs on.
now = datetime.datetime.now()
day= now.strftime('%d_%m_%Y')

# Set the path that the output will be written to. In this case it will be extracted to Aamir's working folder.
outputFolder="G:/Working/Aamir/IandI_General/Back_upv2_" + day + "/"  #where the GDB will be extracted to.

# Attempt to create a folder with the date, if one does not already exists
try:
    os.makedirs(outputFolder)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise
print("Output Folder:"+ outputFolder)

# Define the ArcGIS Pro project that you will be working with.
aprx = arcpy.mp.ArcGISProject(r"G:\Working\Aamir\IandI_General\Back_up\Back_Up_Project_v2\Back_Up_Project_v2.aprx")
mp = aprx.listMaps()[0]

portalurl = 'https://www.arcgis.com'
username = 'wsl_iandi'
password = input("Enter your password")
arcpy.SignInToPortal(portalurl, username, password)

# Create a file geodatabase called LayersBackUp.gdb to store the back-up data. Store the location of this geodatabase in the outputFolder location.
arcpy.CreateFileGDB_management(outputFolder, "LayersBackUp.gdb")
gdbLocation = outputFolder + "\\" + "LayersBackUp.gdb"

# Define the RectifyName function. This function takes the name of an existing layer and replaces invalid characters such as spaces, hyphens and other punctuation with underscores
prohibitedChars = "!@#$%^&*()[]{};:,./<>?\|`~-=+ "
def RectifyName(OldLayerName):
    for char in OldLayerName:
        for letter in prohibitedChars:
            if char == letter:
                OldLayerName = OldLayerName.replace(char, "_")
    return OldLayerName

# Loop through the layers in the project and run the FeatureClassToFeatureClass_conversion tool for each non-basemap, non-group layer.
for lyr in mp.listLayers():
    if lyr.isBasemapLayer == False and lyr.isGroupLayer == False:

        # If the layer is a non-group layer but comes under a group layer, give it a name in the format newgrouplyrname_newlyrname.
        # While saving to a geodatabase, we want to avoid space characters. Hence we replace each space in each layer's name with an underscore character
        fullName = ""
        unsplitName = lyr.longName
        splitArray = unsplitName.split(os.sep)
        #print(lyr.longName)
        for label in splitArray:
            fullName += label + "_"
        fullName = fullName[:-1]
        print(fullName)
        newlyrname = RectifyName(fullName)
        arcpy.FeatureClassToFeatureClass_conversion(lyr, gdbLocation , newlyrname)
        print("Completed: ", newlyrname)
